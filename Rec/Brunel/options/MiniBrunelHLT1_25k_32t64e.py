from Configurables import MiniBrunel

mbrunel = MiniBrunel()

mbrunel.InputData = [ "/dev/shm/MC_Upg_Down_201710_43k.mdf" ]

mbrunel.CallgrindProfile = True
mbrunel.HLT1Only = True

mbrunel.EnableHLTEventLoopMgr = True
mbrunel.HLT1Fitter = True
mbrunel.RunFastForwardFitter = False
mbrunel.PrPixelTriggerMode = True
mbrunel.GECCut = 11500
#mbrunel.IPCut = 0.1

mbrunel.EvtMax = 25000
#mbrunel.ThreadPoolSize = 2
#mbrunel.EventSlots = 2
#mbrunel.EnableHive = True

mbrunel.ThreadPoolSize = 32
mbrunel.EventSlots = 64
mbrunel.EnableHive = True
