2017-10-17 Brunel v53r1
===
This version uses projects LHCb v43r1, Lbcom v21r1, Rec v22r1, Gaudi v29r0, LCG_91
 (Root 6.10.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*

This version is a development release for 2018 data-taking and upgrade simulations

This version is released on `master` branch. The previous release on `master` branch  was `Brunel v53r0`. This release contains all the changes that were made in `Brunel v52r6` and `Brunel v52r6p1` (released on `2017-patches` branch, see corresponding release notes), as well as the additional changes described in this file.

## Change to compiler support
**As from this release, support for gcc7 is added**  
Note that C++17 is not yet fully supported, waiting for support from ROOT  


## Persistency changes
**[MR LHCb!911] Truncate existing raw files when writing**  
Currently, if a raw file exists, it is not truncated when opened for writing. If the existing file has a larger size, the leftover bytes at the end will be left there and further reading will lead to segvs due to the corrupted file.  
This MR simply adds the O_TRUNC flag to the open call and a test that verifies the correct behaviour.

## Changes to configuration
**[MR !300, Rec!766] Change default pA/AA GEC cut from 80k to 20k Velo clusters**  

## Code optimisations
**[MR !607, LHCb!713] Disable boost pool allocators**  

**Multiple code modernisations, see Rec v22r1 and LHCb v43r1 release notes**

## Bug fixes
**[MR !272, LHCb!815] Avoid bad query to PropertyConfigSvc**  

**[MR Rec!696] Fix Calo counter names broken by MR Rec!627**  

**[MR Rec!659] Fixed track checking in VeloIPResolutionMonitor**  
In case an empty set of tracks was found in the TES, the message reporting that no tracks were there was not displayed.  

**[MR Rec!763, Lbcom!184, LHCB!909] Fix untested StatusCodes uncovered by gaudi/Gaudi!386**  


## Monitoring changes
**[MR Rec!747] Add additional variables to VPTrackMonitor**  


## Changes to tests
**[MR !296] Adapt references to run with AVX enabled**  

**[MR !288] Updated 2017 test to use 2017 data...**  

**[MR !242] Skip expected missing counters in repro2012magdown test**  
With the introduction of counters checking (MR LHCb!763) we have to skip counters that are expected to be missing when several tests share the same reference file. This is the case for the repro2012magdown test

**[MR !243] Update reference to follow MR gaudi/Gaudi!314**  
A side effect of gaudi/Gaudi!314 is to remove some DEBUG messages from Sequences. This MR adapts the readdstfsr reference to this change.


