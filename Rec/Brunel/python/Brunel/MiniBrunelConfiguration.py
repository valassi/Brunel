# @package Brunel
#  High level configuration tools for Brunel
#  @author Marco Cattaneo <Marco.Cattaneo@cern.ch>
#  @date   15/08/2008

__version__ = "v49r2"
__author__  = "Sebastien Ponce <Sebastien.Ponce@cern.ch>"

from Gaudi.Configuration import *
from Brunel.Configuration import Brunel
from Configurables import ( LHCbConfigurableUser, LHCbApp,
                            RecMoniConf, L0Conf, PrVeloUT,
                            LHCb__DetDesc__ReserveDetDescForEvent as ReserveIOV,
                            Gaudi__Hive__FetchDataFromFile as FetchDataFromFile,
                            Rich__Future__RawBankDecoder as RichDecoder,
                            TrackSys, HiveWhiteBoard,
                            EventClockSvc, createODIN,
                            LHCb__Tests__FakeEventTimeProducer as FET,
                            TimelineSvc, CallgrindProfile,
                            MeasurementProviderT_MeasurementProviderTypes__UTLite_,
                            MeasurementProvider, PrPixelTracking )
from GaudiConf import IOHelper

## @class MiniBrunel
#  configurable for the MiniBrunel application
#  @author Sebastien Ponce <sebastien.ponce@cern.ch>
class MiniBrunel(LHCbConfigurableUser):

    # Possible used configurables
    __used_configurables__ = [ Brunel, LHCbApp, RecMoniConf,
                               L0Conf, TrackSys, HiveWhiteBoard,
                               EventClockSvc, createODIN,
                               ReserveIOV, FetchDataFromFile, RichDecoder, FET ]

    # Steering options
    __slots__ = {
        "EvtMax" : 1000,
        "IPCut" : -1.0,
        "GECCut" : -1,
        "IPCut" : -1.0,
        "EnableHive" : True,
        "EnableHLTEventLoopMgr" : False,
        "ThreadPoolSize" : 2,
        "EventSlots" : 2,
        "RunRich" : True,
        "CreateTimeLine" : True,
        "DisableTiming" : True,
        "CallgrindProfile" : False,
        "IntelProfile" : False,
        "VeloOnly" : False,
        "DecodingOnly" : False,
        "HLT1Only" : False,
        "HLT1BestPerf" : False,
        "HLT1ParamKalman" : False,
        "HLT1Fitter" : False,
        "RunFastForwardFitter" : True,
        "RunChecks" : False,
        "TimelineFile" : "timeline.csv",
        "HistogramFile" : "",
        "DDDBtag" : "dddb-20171010",
        "CondDBtag" : "sim-20170301-vc-md100",
        "IgnoreChecksum" : False,
        "InputData" : [ "mdf:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MiniBrunel/MC_Upg_Down_201710_43k.mdf" ],
    }

    _propertyDocDct = {
        "EvtMax" : """ Maximum number of events to process (default 1000) """,
        "IPCut" : """ Value to be used to cut on impact parameter""",
        "GECCut" : """ Value to be used to cut on FT+UT clusters""",
        "IPCut" : """ Value to be used to cut on impact parameter""",
        "EnableHive" : """ Whether to run in Hive mode (default true) """,
        "EnableHLTEventLoopMgr" : """ Use HLTEventLoopMgr instead of the HiveSlimEventLoopMgr and AvalancheScheduler """,
        "ThreadPoolSize" : """ If EnableHive is set, size of the thread pool (default 2) """,
        "EventSlots" : """ If EnableHive is set, number of event slot in the TES (default 2) """,
        "RunRich" : """ Whether to run the Rich reconstruction (default yes) """,
        "CreateTimeLine" : """ Whether to create a timeline csv (default yes) """,
        "DisableTiming" : """ Disables the timing auditor if set (default is False) """,
        "CallgrindProfile" : """ Enables callgrind wakeup instructions (default is False) """,
        "IntelProfile" : """ Enables intel wakeup instructions (default is False) """,
        "HLT1Only" : """ Runs MiniBrunel in HLT1 mode. All other algorithms are removes (including Rich) """,
        "HLT1BestPerf" : """ Runs MiniBrunel with a set of configurations leading to optimal performance. to be used for benchmarks """,
        "VeloOnly" : """ Runs MiniBrunel with Only PixelTracking in the sequence""",
        "DecodingOnly" : """ Runs MiniBrunel with Only the Decodign in the sequence""",
        "HLT1Fitter" : """ Use new HLT1Fitter instead of EventTrackFitter """,
        "HLT1ParamKalmen" : """ In case MiniBrunel is run in HLT1 mode, the ParameterizedKalman filter is used. """,
        "RunFastForwardFitter" : """ When True, run RunFastForwardFitter, otherwise, drop it from the sequence (default is True) """,
        "RunChecks" : """ whether to run MClinking and Prchecker (default is False) """,
        "TimelineFile" : """ Name of the output file for timelines. (default is timeline.csv) """,
        "HistogramFile" : """ Name of the output file for histograms. Overwrites the default when not empty (default is empty) """,
        "DDDBtag" : """ Tag for DDDB (default is dddb-20170301) """,
        "CondDBtag" : """ Tag for CondDB (default is sim-20170301-vc-md100) """,
        "IgnoreChecksum" : """ Disable Checksum checks when reading MDF files """,
        "InputData" : """ List of input files to be used (default is one event file of 100 events) """,
    }

    # hack
    def co(self):
        app = ApplicationMgr()
        for c in allConfigurables:
            # until we have a proper implementation of the IOVLock dependency I need
            # a whitelist
            if c not in ('RichFutureDecode', 'ReserveIOV', 'createODIN', 'FetchDataFromFile'):
                c = allConfigurables[c]
                if hasattr(c, 'ExtraInputs') and '/Event/IOVLock' not in c.ExtraInputs:
                    c.ExtraInputs.append('/Event/IOVLock')
        fetcher = FetchDataFromFile('FetchDataFromFile')
        # FIXME: this line can be removed once we find a way to collapse alternatives
        #        for unmet input dependencies
        fetcher.DataKeys = ['/Event/DAQ/RawEvent']
        if self.getProp("RunRich") and not self.getProp("HLT1Only"):
            richDecode = RichDecoder("RichFutureDecode")
            richDecode.ExtraInputs.append('/Event/IOVLock')
        seq = [fetcher,
               createODIN(),
               ReserveIOV('ReserveIOV')]
        if self.getProp("RunRich") and not self.getProp("HLT1Only"):
            seq.append(richDecode)
        app.TopAlg += seq
        # this is a hack because the raw file we are using does not have ODIN :(
        app.TopAlg.append(FET('DummyEventTime',
                              Start=EventClockSvc().InitialTime / 1E9,
                              Step=0))
        ReserveIOV('ReserveIOV').ODIN = FET('DummyEventTime').ODIN = '/Event/DAQ/DummyODIN'

    ## callgrind profiling
    def profile(self):
        if self.getProp("CallgrindProfile"):
            p = CallgrindProfile()
            p.StartFromEventN = 10
            p.StopAtEventN = self.getProp("EvtMax")
            p.DumpAtEventN = p.StopAtEventN
            ApplicationMgr().TopAlg.insert(0,p)
        if self.getProp("IntelProfile"):
            try:
                from Configurables import IntelProfile
            except ImportError:
                raise ImportError("Can't import IntelProfile configurable! Did you forget to setup the intel profiler before compiling?")
            p = IntelProfile()
            p.StartFromEventN = 10
            p.StopAtEventN = self.getProp("EvtMax")
            ApplicationMgr().TopAlg.insert(0,p)

    ## cleanup for an HLT1 sequence
    def hlt1only(self):
        for a in list(AuditorSvc().Auditors):
            if a == 'ChronoAuditor' or (hasattr(a, 'name') and a.name() == 'ChronoAuditor'):
                AuditorSvc().Auditors.remove(a)
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(GaudiSequencer("TrackAddExtraInfoSeq"))
        except ValueError:
            None
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(GaudiSequencer("BestTrackCreatorSeq"))
        except ValueError:
            None
        # drop the RawBankToSTClusterAlg/createUTClusters algorithm from the HLT1 sequence as it's not needed
        # make sure the ForwardFitterAlgFast knows about it
        GaudiSequencer('RecoDecodingSeq').Members.remove(GaudiSequencer('createUTClusters'))
        if "ParameterizedKalman" in TrackSys().getProp("ExpertTracking"):
          GaudiSequencer("ForwardFitterAlgParamFast").MeasProvider_UT = MeasurementProviderT_MeasurementProviderTypes__UTLite_()
        elif self.getProp("HLT1Fitter"):
          GaudiSequencer("ForwardFitterAlgFast").MeasProvider.UTProvider = MeasurementProviderT_MeasurementProviderTypes__UTLite_()
        else:
          GaudiSequencer("ForwardFitterAlgFast").Fitter.MeasProvider.UTProvider = MeasurementProviderT_MeasurementProviderTypes__UTLite_()

    ## best performance options for HLT1
    def hlt1BestPerf(self):
        from Configurables import PrPixelTracking, PrVeloUT, PrForwardTracking, RawBankToSTLiteClusterAlg
        from GaudiKernel.SystemOfUnits import GeV
        from GaudiKernel.SystemOfUnits import mm
        try:
            PrPixelTracking("PrPixelTrackingFast").AlgoConfig = "ForwardThenBackward"
            PrPixelTracking("PrPixelTrackingFast").HardFlagging = True
            PrPixelTracking("PrPixelTrackingFast").SkipLoopSens = True
            PrPixelTracking("PrPixelTrackingFast").MaxMissedOnTrack = 2
            PrPixelTracking("PrPixelTrackingFast").MaxMissedConsecutive = 1
            PrPixelTracking("PrPixelTrackingFast").PhiWindow = 2.5
            PrPixelTracking("PrPixelTrackingFast").PhiWindowExtrapolation = 2.5
            PrPixelTracking("PrPixelTrackingFast").ModulesToSkip = []
            PrPixelTracking("PrPixelTrackingFast").EarlyKill3HitTracks = True
            #IPCut in Velo UT and PT Thresholds
            PrVeloUT("PrVeloUTFast").doIPCut = True
            PrVeloUT("PrVeloUTFast").minIP = 0.1*mm
            PrVeloUT("PrVeloUTFast").minPT = 0.800*GeV
            #PT Threshold for Forward tracking algo
            GaudiSequencer("PrForwardTrackingFast").PrForwardTool.MinPT = 1.0*GeV
            # skip errors in createUTLiteClusters
            RawBankToSTLiteClusterAlg("createUTLiteClusters").skipBanksWithErrors = True
        except ValueError:
            print "HLT1 fast sequence badly configured, try again"

    ## cleanup for an HLT1 sequence
    def donotrunfastforwardfitter(self):
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(GaudiSequencer("ForwardFitterAlgFast"))
        except ValueError:
            None

    ## set the IP cut value
    def setIPCut(self):
        PrVeloUT("PrVeloUTFast").doIPCut = True
        PrVeloUT("PrVeloUTFast").minIP = self.getProp("IPCut")

    def decodingonly(self):
        GaudiSequencer("RecoTrFastSeq").Members =[]

    def veloonly(self):
        #no decoding of FT and UT
        GaudiSequencer("RecoDecodingSeq").Members = []
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove( GaudiSequencer("PrVeloUTFast") )
            GaudiSequencer("RecoTrFastSeq").Members.remove( GaudiSequencer("PatPV3D") )
            GaudiSequencer("RecoTrFastSeq").Members.remove( GaudiSequencer("PrForwardTrackingFast") )
        except ValueError:
            None
    ## put everything in one sequence so PrGECFilter works
    def fixGECForBenchmark(self):
        if self.getProp("HLT1Only"):
            try:
                FastSeq = GaudiSequencer("RecoTrFastSeq")
                Parallel = GaudiSequencer("ParallelSeq")
                Filter = GaudiSequencer("FilterSeq", ModeOR=False, ShortCircuit=True, Sequential=True)

                PrStoreFT = GaudiSequencer("RecoDecodingSeq").Members.pop(-1)
                PrStoreUT = GaudiSequencer("RecoDecodingSeq").Members.pop(-1)
                PrGECFilter = GaudiSequencer("RecoDecodingSeq").Members.pop(-1)
                PrGECFilter.NumberFTUTClusters = self.getProp("GECCut")

                Parallel.Members = [PrStoreFT, PrStoreUT]
                Parallel.Members.extend(FastSeq.Members)
                Filter.Members = [PrGECFilter, Parallel]

                GaudiSequencer("RecoDecodingSeq").Members.append(Filter)
                FastSeq.Members=[]
            except ValueError:
                None

    ## set the IP cut value
    def setIPCut(self):
        PrVeloUT("PrVeloUTFast").doIPCut = True
        PrVeloUT("PrVeloUTFast").minIP = self.getProp("IPCut")

    ## Apply the configuration
    def __apply_configuration__(self):
        brunel = Brunel()
        brunel.DataType = "Upgrade"
        brunel.EvtMax = self.getProp("EvtMax")
        brunel.DatasetName="future"
        brunel.InitSequence = []
        if not self.getProp("RunRich") or self.getProp("HLT1Only"):
            brunel.MainSequence = ["ProcessPhase/Reco"]
            brunel.RecoSequence = ["Decoding","TrFast"]
        else:
            brunel.MainSequence = ["ProcessPhase/Reco","ProcessPhase/Moni"]
            brunel.RecoSequence = ["Decoding", "TrFast", "TrBest", "RICH"]
            RecMoniConf().MoniSequence = ["RICH"]
        if self.getProp("RunChecks"):
            brunel.MainSequence.append('ProcessPhase/MCLinks')
            brunel.MainSequence.append('ProcessPhase/Check')
            brunel.WithMC = True
        brunel.Detectors = ["VP", "UT", "FT"]
        brunel.OutputType = "NONE"
        brunel.PackType = "NONE"
        brunel.Simulation = True
        brunel.InputType = 'DIGI'
        brunel.DDDBtag = self.getProp("DDDBtag")
        brunel.CondDBtag = self.getProp("CondDBtag")
        brunel.DisableTiming = self.getProp("DisableTiming")

        L0Conf().EnsureKnownTCK=False

        TrackSys().TrackingSequence = ["TrFast", "TrBest"]
        if self.getProp("HLT1Fitter"):
            TrackSys().TrackingSequence.append("TrHlt")
        TrackSys().TrackTypes = ["Velo","Upstream","Forward","Downstream","Seeding","Match"]
        TrackSys().ExpertTracking = [ "simplifiedGeometry" ]
        if self.getProp("HLT1ParamKalman"):
          TrackSys().ExpertTracking.append("ParameterizedKalman")

        LHCbApp().EnableHive = self.getProp("EnableHive")
        if LHCbApp().EnableHive:
            whiteboard = HiveWhiteBoard("EventDataSvc")
            whiteboard.EventSlots = self.getProp("EventSlots")
        LHCbApp().EnableHLTEventLoopMgr = self.getProp("EnableHLTEventLoopMgr")
        LHCbApp().ThreadPoolSize = self.getProp("ThreadPoolSize")
        EventClockSvc(InitialTime=1433509200000000000)

        if LHCbApp().EnableHive:
            appendPostConfigAction(self.co)
        if self.getProp("CallgrindProfile") or self.getProp("IntelProfile"):
            appendPostConfigAction(self.profile)
        if self.getProp("HLT1Only"):
            appendPostConfigAction(self.hlt1only)
        if self.getProp("HLT1BestPerf"):
            appendPostConfigAction(self.hlt1BestPerf)
        if not self.getProp("RunFastForwardFitter"):
            appendPostConfigAction(self.donotrunfastforwardfitter)
        if self.getProp("IPCut") > 0:
            appendPostConfigAction(self.setIPCut)

        # This allows correct use of the GEC filter, but only for now.
        #FIXME The real fix is to rework how we define the tracking sequence
        if self.getProp("GECCut") > 0:
            appendPostConfigAction(self.fixGECForBenchmark)

        if self.getProp("VeloOnly"):
            appendPostConfigAction( self.veloonly)
        if self.getProp("DecodingOnly"):
            appendPostConfigAction( self.decodingonly)

        EvtSelector = IOHelper("MDF").inputFiles(self.getProp("InputData"))
        if self.getProp("IgnoreChecksum"):
            inputs = []
            for inp in EvtSelector.Input:
                inputs.append(inp +" IgnoreChecksum='YES'")
            EvtSelector.Input = inputs

        if self.getProp("CreateTimeLine"):
            TimelineSvc(RecordTimeline=True, DumpTimeline=True, TimelineFile=self.getProp("TimelineFile"))
        if self.getProp("HistogramFile"):
            HistogramPersistencySvc().OutputFile = self.getProp("HistogramFile")
        if self.getProp("RunChecks"):
            # FIXME CheckPatSeq is failing right now. As we do not need it, we drop it for the moment
            def dropSeqPatCheck():
                GaudiSequencer("CheckPatSeq").Members.pop(-1)
            appendPostConfigAction(dropSeqPatCheck)
