import os
import Gaudi.Configuration
from Configurables import Brunel

brunel = Brunel()
brunel.DataType = "2016"

from Configurables import EventSelector
EventSelector().PrintFreq = 100
