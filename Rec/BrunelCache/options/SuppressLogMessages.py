# -*- coding: utf-8 -*-
from Gaudi.Configuration import log
from GaudiKernel.Configurable import log as clog
import logging
import re

IGNORED_MESSAGES = map(re.compile,
                       (# errors
                        # warnings
                        r'Using default tag.*for partition',
                        r'something else configured a decoder already',
                        r'Property .* is set in both'
                       ))

class MessageFilter(logging.Filter):
    def filter(self, record):
        if record.levelno >= logging.WARNING:
            if any(exp.search(record.msg) for exp in IGNORED_MESSAGES):
                return False
            if not record.msg.strip():
                return False # why should anyone want to print an empty warning?
        return True

for l in (log, clog):
    l.addFilter(MessageFilter())

